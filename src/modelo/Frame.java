package modelo;

public class Frame {

	/**
	 * Um objeto do tipo frame possui o conteúdo que estava dentro do arquivo
	 * referenciado na página
	 */

	Object conteudo;
	
	public Object getConteudo() {
		return conteudo;
	}

	public void setConteudo(Object conteudo) {
		this.conteudo = conteudo;
	}
}
