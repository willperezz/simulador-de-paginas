package sample;

import controle.SistemaOperacional;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import substituicao.Fifo;
import substituicao.Substituicao;

import java.util.Scanner;

/**
 * Classe que produz o gráfico das taxas de hit e miss, e recebe no inicio os
 * valores de tamanho das memórias e das páginas
 * 
 * @author willperez
 *
 */
public class Janela extends Application {

	public static Data miss;
	public static Data hit;
	private int hits;
	private int misses;
	public Label taxa;
	public static ObservableList<Data> pieChartData;

	@Override
	public void start(Stage stage) {

		int tamanhoPagina = 0;
		int tamanhoFisica = 0;
		int tamanhoVirtual = 0;

		Scanner leitor = new Scanner(System.in);

		boolean confirma = false;
		while (!confirma) {
			System.out.println("--------------------------------------------------");
			System.out.println("Digite o tamanho da Página e do Frame (KB):");
			tamanhoPagina = leitor.nextInt();
			System.out.println("Digite o tamanho da Memória Física (KB):");
			tamanhoFisica = leitor.nextInt();
			System.out.println("Digite o tamanho da Memória Virtual (KB):");
			tamanhoVirtual = leitor.nextInt();

			if (tamanhoPagina > tamanhoFisica) {
				System.out.println("Tamanho da página maior do que a Memória Física!");
			} else if (tamanhoPagina > tamanhoVirtual) {
				System.out.println("Tamanho da página maior do que a Memória Virtual!");
			} else if (tamanhoPagina < 1 || tamanhoFisica < 1 || tamanhoVirtual < 1) {
				System.out.println("Os tamanhos devem ser maiores do que 0");
			} else {
				confirma = true;
			}
		}

		int numeroPosicoesFisica = (Integer) tamanhoFisica / tamanhoPagina;
		System.out.println("A memória física tem " + numeroPosicoesFisica + " posições");
		int numeroPosicoesVirtual = (Integer) tamanhoVirtual / tamanhoPagina;
		System.out.println("A memória lógica tem " + numeroPosicoesVirtual + " posições");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite os quadros: ");
		String referencia = scanner.nextLine();
		String[] stringReferencia = referencia.split(";");
		Substituicao fifo = new Fifo(3);
		for (int i = 0; i < (stringReferencia.length - 1); i++) {
			fifo.inserirPagina(stringReferencia[i]);

		}
		System.out.println("Falhas: " + fifo.getContagemDeFalhas());


		SistemaOperacional sistema = new SistemaOperacional(numeroPosicoesVirtual, numeroPosicoesFisica, tamanhoPagina);
		Thread threadSistema = new Thread(sistema);

		threadSistema.start();

	}

	public static void main(String[] args) {
		launch(args);
	}

	public void setTaxaText(String texto) {
		this.taxa.setText(texto);

	}
}