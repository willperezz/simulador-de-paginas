package substituicao;

import java.util.LinkedList;

public class Fifo extends Substituicao {
    private static int INSERCAO = 0;

    public Fifo(int numeroDeQuadros) {

        super(numeroDeQuadros);
        this.ListQuadros = new LinkedList();
    }

    @Override
    public void inserirPagina(String pageNumber) {
        // antes de inserir, checar se a pagina ja esta na lista
        if (!ListQuadros.contains(pageNumber)) {

            // se a quantidade de paginas na memoria for menor que o numero de
            // quadros
            // ou seja, ainda ha espaco
            if (ListQuadros.size() < numDeQuadros) {
                ListQuadros.add(pageNumber);
            } else {
                ListQuadros.remove(INSERCAO);
                ListQuadros.add(INSERCAO, pageNumber);
                INSERCAO++;
                if (INSERCAO == numDeQuadros) {
                    INSERCAO = 0;
                }
            }
            numDeFalhas++;
        }
    }
}
