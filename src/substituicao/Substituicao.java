package substituicao;

import java.util.LinkedList;

abstract class Substituicao {
    protected int numDeFalhas;
    protected int numDeQuadros;
    LinkedList ListQuadros;

    public Substituicao(int numDeQuadros) {
        if (numDeQuadros < 0)
            throw new IllegalArgumentException();
            this.numDeQuadros = numDeQuadros;
            numDeFalhas = 0;
    }

    public int getContagemDeFalhas() {
      return numDeFalhas;
    }

    public abstract void inserirPagina(String pagina);

    public void imprimirQuadros() {
        System.out.println("Quadros: ");
        for (int i = 0; i< ListQuadros.size(); i++) {
            System.out.println(ListQuadros.get(i) + "");
        }
        System.out.println();
    }


}
